<?php

class Paginas extends Controller

{
    public function __construct()
    {

        // Desde aquí cargaremos los modelos // ------------->


    }

    public function index()
    {

        $data = [
            'titulo' => 'Mini_MVC by Manuel Martín'
        ];

        return $this->view('paginas/index', $data);
    }
}
