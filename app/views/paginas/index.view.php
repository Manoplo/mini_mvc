<?php require_once APPROOT . '/views/partials/header.php'; ?>

<h1 style="font-family : sans-serif"><?= isset($data['titulo']) ? $data['titulo'] : ''; ?></h1>
<h4 style="font-family : sans-serif">Make the world a better place from here</h4>

<?php require_once APPROOT . '/views/partials/footer.php'; ?>